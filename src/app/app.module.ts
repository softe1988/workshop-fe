import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MascaraComponent } from './mascara/mascara.component';
import { LipstickComponent } from './lipstick/lipstick.component';
import { MaterializeModule } from 'ng2-materialize';


@NgModule({
  declarations: [
    AppComponent,
    MascaraComponent,
    LipstickComponent
  ],
  imports: [
    BrowserModule,
    MaterializeModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
