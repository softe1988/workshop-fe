import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mascara',
  templateUrl: './mascara.component.html',
  styleUrls: ['./mascara.component.css']
})
export class MascaraComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  brand: string;
  name: string;
  color: string;
  description: string;
  itemType: string;
  price: string;
  //userFile.data: any; 

}
