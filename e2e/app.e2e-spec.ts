import { WorkshopFEPage } from './app.po';

describe('workshop-fe App', () => {
  let page: WorkshopFEPage;

  beforeEach(() => {
    page = new WorkshopFEPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
